//------------------------------------------------------------------
//                Déclaration des modules
//------------------------------------------------------------------
const mysql = require('mysql');
var serialport = require('serialport');
var requete = require('request');
const { log } = require('console');
var portArduino = "/dev/cu.usbmodemFD131";
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var { SerialPort } = require('serialport');
const { ReadlineParser } = require('@serialport/parser-readline');
const nodemailer = require('nodemailer');

//------------------------------------------------------------------
//
//                           Configuration nodemailer
//
//------------------------------------------------------------------

// Configurer le transporteur
let transporter = nodemailer.createTransport({
   host: 'smtp.office365.com',
   auth: {
       user: 'compostaubigny@outlook.fr', // Entrez votre adresse e-mail Gmail : compostaubigny@gmail.com, outlook : compostaubigny@outlook.fr
       pass: '1[P73X|g{n' // mot de passe : 1[P73X|g{n , appli gmail : lqym ofvb oeyc ynii
   },
   port: 587, // Utilisation du port 587 pour STARTTLS
   secure: false, // Ne pas utiliser SSL/TLS explicite
   tls: {
      ciphers: 'SSLv3'
   },
   timeout: 30000
});

//------------------------------------------------------------------
//
//                           ESP32 LoRa
//
// Dès réception d'une data, celle-ci est découpé et mémorisée dans la base de donnée
//------------------------------------------------------------------


// Sélection du port série
var SP1 = new SerialPort({
   path: '/dev/ttyACM0',
   baudRate: 115200
},
)

//-------événement sur l'ouverture du port SP1 ------------------
SP1.on('open', function () {
   console.log('Port com ouvert...')
})

//-------  création du parser "retour ligne"   ------------------
var parser = new ReadlineParser();
// ------------------On crée le flux SP1--->parser---------------
SP1.pipe(parser)
//----- événement sur une récéption de données sur le parser ----
parser.on('data', function (data) {
   console.log('Donnée reçus', data);

   var DonneeCapteur = JSON.parse(data);
   var T1 = DonneeCapteur.T1;
   var T2 = DonneeCapteur.T2;
   var T3 = DonneeCapteur.T3;
   var T4 = DonneeCapteur.T4;
   var T5 = DonneeCapteur.T5;
   var T6 = DonneeCapteur.T6;
   var Te = DonneeCapteur.Te;

   var H1 = DonneeCapteur.H1;
   var H2 = DonneeCapteur.H2;
   var H3 = DonneeCapteur.H3;
   var H4 = DonneeCapteur.H4;
   var H5 = DonneeCapteur.H5;
   var H6 = DonneeCapteur.H6;
   var He = DonneeCapteur.He;

   var Nb = DonneeCapteur.Nb;

   const now = new Date();
   // Obtenez l'année, le mois et le jour
   const year = now.getFullYear();
   const month = String(now.getMonth() + 1).padStart(2, '0'); // Les mois sont indexés de 0 à 11, donc ajoutez 1
   const day = String(now.getDate()).padStart(2, '0');

   var date = `${year}-${month}-${day}`;
   var heure = now.getHours();

    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'compost',
      password: 'MdpCompost',
      database: 'BDD_compost_aubigny'
   });

   // Connexion à la base de données
   connection.connect((err) => {
      if (err) {
         console.error('Erreur de connexion à la base de données :', err);
         throw err;
      }
      console.log('Connecté à la base de données MySQL');
   });


   // Exécutez la requête SELECT
   connection.query("INSERT INTO mesures_Temperature (T1 ,T2 ,T3 ,T4 ,T5 ,T6 ,Te ,date ,heure) VALUES (" + T1 + ',' + T2 + ',' + T3 + ',' + T4 + ',' + T5 + ',' + T6 + ',' + Te + ",'" + date + "'," + heure + ')', (err, results, fields) => {
      if (err) throw err;
   });

   connection.query("INSERT INTO mesures_Hygrometrie (H1 ,H2 ,H3 ,H4 ,H5 ,H6 ,He ,date ,heure) VALUES (" + H1 + ',' + H2 + ',' + H3 + ',' + H4 + ',' + H5 + ',' + H6 + ',' + He + ",'" + date + "'," + heure + ')', (err, results, fields) => {
      if (err) throw err;
   });

   connection.query("INSERT INTO mesures_NiveauBatterie (Nb,date ,heure) VALUES (" + Nb + ",'" + date + "'," + heure + ')', (err, results, fields) => {
      if (err) throw err;
   });

   connection.query("SELECT * FROM alert_compost LIMIT 1", (err, results, fields) => {
      if (err) throw err;

      console.log('Information de mail: ', results);
      if(results.length != 0){
         if((T1 < results[0].tmin ||T2 < results[0].tmin ||T3 < results[0].tmin ||T4 < results[0].tmin ||T5 < results[0].tmin ||T6 < results[0].tmin )||(T1 > results[0].tmax ||T2 > results[0].tmax ||T3 > results[0].tmax ||T4 > results[0].tmax ||T5 > results[0].tmax ||T6 > results[0].tmax )||(H1 < results[0].hmin ||H2 < results[0].hmin ||H3 < results[0].hmin ||H4 < results[0].hmin ||H5 < results[0].hmin ||H6 < results[0].hmin )||(H1 > results[0].hmax ||H2 > results[0].hmax ||H3 > results[0].hmax ||H4 > results[0].hmax ||H5 > results[0].hmax ||H6 > results[0].hmax)||Nb<results[0].nb){
            console.log("Préparation de l'E-mail");
            // Définir les données pour l'e-mail 
            let destinataire = results[0].email;
            let sujet = 'Alerte Compost';
            let messageFinal = `
            <html>
            <head>
                <style>
                    /* Appliquer la police Montserrat au texte */
                    p {
                     font-family: sans-serif;
                     }
                </style>
            </head>
            <body>
               <h1>Bonjour ${results[0].prenom} ${results[0].nom},</h1>
        `;
            
            
            if(T1 < results[0].tmin ||T2 < results[0].tmin ||T3 < results[0].tmin ||T4 < results[0].tmin ||T5 < results[0].tmin ||T6 < results[0].tmin){
               let messageTmin = `<p>Notre système a détecté que la  <span style="color: red;"> température est passée sous le seuil d\'alerte.</span> La température d\'un des capteurs est de ${Math.min(T1,T2,T3,T4,T5,T6)}°C.</p>`;
               messageFinal = messageFinal + messageTmin;
            }
            if(T1 > results[0].tmax ||T2 > results[0].tmax ||T3 > results[0].tmax ||T4 > results[0].tmax ||T5 > results[0].tmax ||T6 > results[0].tmax ){
               let messageTmax = `<p>Notre système a détecté que la <span style="color: red;"> température a dépassé le seuil d\'alerte.</span> La température d\'un des capteurs est de ${Math.max(T1,T2,T3,T4,T5,T6)}°C.</p>`;
               messageFinal = messageFinal + messageTmax;
            }
            if(H1 < results[0].hmin ||H2 < results[0].hmin ||H3 < results[0].hmin ||H4 < results[0].hmin ||H5 < results[0].hmin ||H6 < results[0].hmin ){
               let messageHmin = `<p>Notre système a détecté que le <span style="color: red;">niveau d\'hygrométrie est passée sous le seuil d\'alerte.</span> Le niveau d\'hygrométrie d\'un des capteurs est de ${Math.min(H1,H2,H3,H4,H5,H6)}%.</p>`;
               messageFinal = messageFinal + messageHmin;
            }
            if(H1 > results[0].hmax ||H2 > results[0].hmax ||H3 > results[0].hmax ||H4 > results[0].hmax ||H5 > results[0].hmax ||H6 > results[0].hmax ){
               let messageHmax = `<p>Notre système a détecté que le <span style="color: red;">niveau d\'hygrométrie a dépassé le seuil d\'alerte.</span> La niveau d\'hygrométrie d\'un des capteurs est de ${Math.max(H1,H2,H3,H4,H5,H6)}%.</p>`;
               messageFinal = messageFinal + messageHmax;
            }
            if(Nb<results[0].nb){
               let messageNb = `<p>Notre système a détecté que le <span style="color: red;">niveau de la batterie est passée sous le seuil d\'alerte.</span> La niveau de la batterie est de ${Nb}%.</p>`;
               messageFinal = messageFinal + messageNb;
            }

            let messageFin = `
               <p>Pour plus d\'information vous pouvez allez sur le site du compost: http://193.253.222.162:2180</p>
               </body>
               </html>
            `;
            messageFinal = messageFinal + messageFin;

            // Configurer l'e-mail
            let mailOptions = {
               from: 'compostaubigny@outlook.fr', // Adresse e-mail d'envoi
               to: destinataire, // Adresse e-mail du destinataire
               subject: sujet, // Sujet de l'e-mail
               html: messageFinal // Corps de l'e-mail au format HTML
            };
         
            // Envoyer l'e-mail
            transporter.sendMail(mailOptions, function(error, info) {
               if (error) {
                  console.log('E-mail ' +error);
               } else {
                  console.log('E-mail envoyé: ' + info.response);
               }
            });
         }
      }
   });


   // Fermeture de la connexion à la base de données
   connection.end((err) => {
      if (err) {
         console.error('Erreur lors de la fermeture de la connexion :', err);
         throw err;
      }
      console.log('Connexion à la base de données fermée.');
   });


})