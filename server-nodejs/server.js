const express = require('express');
const cors = require('cors');
const app = express();
const fs = require("fs");
const bodyParser = require('body-parser');
const ip = require('ip');
const _ = require('lodash');
const mysql = require('mysql');

app.use(cors());


//enable CORS for request verbs
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
  next();
});

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

// Fonction pour formater la date et l'heure
const formaterDateTime = (dateISO, heureInt) => {
  const dateObj = new Date(dateISO);
  const mois = (dateObj.getMonth() + 1).toString().padStart(2, '0'); // Obtenez le mois, ajoutez des zéros au début si nécessaire
  const jour = dateObj.getDate().toString().padStart(2, '0'); // Obtenez le jour, ajoutez des zéros au début si nécessaire
  const heureFormatee = heureInt.toString().padStart(2, '0'); // Ajouter des zéros au début si nécessaire
  return `${mois}-${jour} ${heureFormatee}H`;
};

//Handle GET method for listing Hygrometrie
app.get('/HygrometrieSemaine', function (req, res) {

  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'compost',
    password: 'MdpCompost',
    database: 'BDD_compost_aubigny'
  });

  connection.on('connect', () => {
    console.log('Connecté à la base de données MySQL');

    // Exécutez la requête SELECT
    connection.query('SELECT * FROM mesures_Hygrometrie ORDER BY id DESC LIMIT 168 ', (err, results, fields) => {
      if (err) {
        console.error('Erreur lors de l\'exécution de la requête SELECT :', err);
        throw err;
      }

      // Parcourir le tableau et formater les dates
      const tableauFormate = results.map(element => {
        // Vérifier si l'élément est une date
        if (element.date) {
          // Formater la date
          return { ...element, date: formaterDateTime(element.date, element.heure) };
        } else {
          // Renvoyer l'élément tel quel s'il ne s'agit pas d'une date
          return element;
        }
      });


      // Traitez les résultats de la requête SELECT
      console.log('Résultats de la requête SELECT :', JSON.stringify(tableauFormate));
      res.end(JSON.stringify(tableauFormate));

      // Fermez la connexion à la base de données après avoir traité les résultats
      connection.end();
    });
  });

  // Écoutez l'événement d'erreur de connexion
  connection.on('error', (err) => {
    console.error('Erreur de connexion à la base de données :', err);
  });

  // Connectez-vous à la base de données
  connection.connect();

})

//Handle GET method for listing Hygrometrie
app.get('/HygrometrieMois', function (req, res) {

  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'compost',
    password: 'MdpCompost',
    database: 'BDD_compost_aubigny'
  });

  connection.on('connect', () => {
    console.log('Connecté à la base de données MySQL');

    // Exécutez la requête SELECT
    connection.query('SELECT * FROM mesures_Hygrometrie ORDER BY id DESC LIMIT 10080 ', (err, results, fields) => {
      if (err) {
        console.error('Erreur lors de l\'exécution de la requête SELECT :', err);
        throw err;
      }

      // Parcourir le tableau et formater les dates
      const tableauFormate = results.map(element => {
        // Vérifier si l'élément est une date
        if (element.date) {
          // Formater la date
          return { ...element, date: formaterDateTime(element.date, element.heure) };
        } else {
          // Renvoyer l'élément tel quel s'il ne s'agit pas d'une date
          return element;
        }
      });


      // Traitez les résultats de la requête SELECT
      console.log('Résultats de la requête SELECT :', JSON.stringify(tableauFormate));
      res.end(JSON.stringify(tableauFormate));

      // Fermez la connexion à la base de données après avoir traité les résultats
      connection.end();
    });
  });

  // Écoutez l'événement d'erreur de connexion
  connection.on('error', (err) => {
    console.error('Erreur de connexion à la base de données :', err);
  });

  // Connectez-vous à la base de données
  connection.connect();

})

//Handle GET method for listing Temperature
app.get('/TemperatureSemaine', function (req, res) {

  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'compost',
    password: 'MdpCompost',
    database: 'BDD_compost_aubigny'
  });

  connection.on('connect', () => {
    console.log('Connecté à la base de données MySQL');

    // Exécutez la requête SELECT
    connection.query('SELECT * FROM mesures_Temperature ORDER BY id DESC LIMIT 168', (err, results, fields) => {
      if (err) {
        console.error('Erreur lors de l\'exécution de la requête SELECT :', err);
        throw err;
      }


      // Parcourir le tableau et formater les dates
      const tableauFormate = results.map(element => {
        // Vérifier si l'élément est une date
        if (element.date) {
          // Formater la date
          return { ...element, date: formaterDateTime(element.date, element.heure) };
        } else {
          // Renvoyer l'élément tel quel s'il ne s'agit pas d'une date
          return element;
        }
      });


      // Traitez les résultats de la requête SELECT
      console.log('Résultats de la requête SELECT :', JSON.stringify(tableauFormate));
      res.end(JSON.stringify(tableauFormate));

      // Fermez la connexion à la base de données après avoir traité les résultats
      connection.end();
    });
  });

  // Écoutez l'événement d'erreur de connexion
  connection.on('error', (err) => {
    console.error('Erreur de connexion à la base de données :', err);
  });

  // Connectez-vous à la base de données
  connection.connect();

})

//Handle GET method for listing Temperature
app.get('/TemperatureMois', function (req, res) {

  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'compost',
    password: 'MdpCompost',
    database: 'BDD_compost_aubigny'
  });

  connection.on('connect', () => {
    console.log('Connecté à la base de données MySQL');

    // Exécutez la requête SELECT
    connection.query('SELECT * FROM mesures_Temperature ORDER BY id DESC LIMIT 10080', (err, results, fields) => {
      if (err) {
        console.error('Erreur lors de l\'exécution de la requête SELECT :', err);
        throw err;
      }

      // Parcourir le tableau et formater les dates
      const tableauFormate = results.map(element => {
        // Vérifier si l'élément est une date
        if (element.date) {
          // Formater la date
          return { ...element, date: formaterDateTime(element.date, element.heure) };
        } else {
          // Renvoyer l'élément tel quel s'il ne s'agit pas d'une date
          return element;
        }
      });


      // Traitez les résultats de la requête SELECT
      console.log('Résultats de la requête SELECT :', JSON.stringify(tableauFormate));
      res.end(JSON.stringify(tableauFormate));

      // Fermez la connexion à la base de données après avoir traité les résultats
      connection.end();
    });
  });

  // Écoutez l'événement d'erreur de connexion
  connection.on('error', (err) => {
    console.error('Erreur de connexion à la base de données :', err);
  });

  // Connectez-vous à la base de données
  connection.connect();

})

//Handle GET method for listing NiveauBatterie
app.get('/NiveauBatterie', function (req, res) {

  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'compost',
    password: 'MdpCompost',
    database: 'BDD_compost_aubigny'
  });

  connection.on('connect', () => {
    console.log('Connecté à la base de données MySQL');

    // Exécutez la requête SELECT
    connection.query('SELECT * FROM mesures_NiveauBatterie ORDER BY id desc LIMIT 1', (err, results, fields) => {
      if (err) {
        console.error('Erreur lors de l\'exécution de la requête SELECT :', err);
        throw err;
      }

      // Parcourir le tableau et formater les dates
      const tableauFormate = results.map(element => {
        // Vérifier si l'élément est une date
        if (element.date) {
          // Formater la date
          return { ...element, date: formaterDateTime(element.date, element.heure) };
        } else {
          // Renvoyer l'élément tel quel s'il ne s'agit pas d'une date
          return element;
        }
      });


      // Traitez les résultats de la requête SELECT
      console.log('Résultats de la requête SELECT :', JSON.stringify(tableauFormate));
      res.end(JSON.stringify(tableauFormate));

      // Fermez la connexion à la base de données après avoir traité les résultats
      connection.end();
    });
  });

  // Écoutez l'événement d'erreur de connexion
  connection.on('error', (err) => {
    console.error('Erreur de connexion à la base de données :', err);
  });

  // Connectez-vous à la base de données
  connection.connect();

})

//Handle GET method for listing NiveauBatterie
app.get('/GetAlert', function (req, res) {

  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'compost',
    password: 'MdpCompost',
    database: 'BDD_compost_aubigny'
  });

  connection.on('connect', () => {
    console.log('Connecté à la base de données MySQL');

    // Exécutez la requête SELECT
    connection.query('SELECT * FROM alert_compost ORDER BY id LIMIT 1', (err, results, fields) => {
      if (err) {
        console.error('Erreur lors de l\'exécution de la requête SELECT :', err);
        throw err;
      }

      // Traitez les résultats de la requête SELECT
      console.log('Résultats de la requête SELECT :', JSON.stringify(results));
      res.end(JSON.stringify(results));

      // Fermez la connexion à la base de données après avoir traité les résultats
      connection.end();
    });
  });

  // Écoutez l'événement d'erreur de connexion
  connection.on('error', (err) => {
    console.error('Erreur de connexion à la base de données :', err);
  });

  // Connectez-vous à la base de données
  connection.connect();

})

//Handle POST method
app.post('/AddAlert', function (req, res) {

  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'compost',
    password: 'MdpCompost',
    database: 'BDD_compost_aubigny'
  });

  connection.on('connect', () => {
    console.log('Connecté à la base de données MySQL');
    data = { nom: req?.body?.nom, prenom: req?.body?.prenom, email: req?.body?.email, tmin: req?.body?.tmin, tmax: req?.body?.tmax, hmin: req?.body?.hmin, hmax: req?.body?.hmax, nb: req?.body?.nb };
    console.log('Data a ajouter :', data);
    // Exécutez la requête ADD
    connection.query("INSERT INTO alert_compost (nom, prenom, email, tmin, tmax, hmin, hmax, nb) VALUES ('" + req?.body?.nom + "','" + req?.body?.prenom + "','" + req?.body?.email + "'," + req?.body?.tmin + ',' + req?.body?.tmax + ',' + req?.body?.hmin + ',' + req?.body?.hmax + ',' + req?.body?.nb + ')', (err, results, fields) => {
      if (err) {
        console.error('Erreur lors de l\'exécution de la requête ADD :', err);
        throw err;
      }

      // Traitez les résultats de la requête SELECT
      console.log('Résultats de la requête ADD :', JSON.stringify(results));
      res.end(JSON.stringify(results));

      // Fermez la connexion à la base de données après avoir traité les résultats
      connection.end();
    });
  });

  // Écoutez l'événement d'erreur de connexion
  connection.on('error', (err) => {
    console.error('Erreur de connexion à la base de données :', err);
  });

  // Connectez-vous à la base de données
  connection.connect();

})

//Handle DELETE method
app.delete('/DeleteAlert', function (req, res) {

  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'compost',
    password: 'MdpCompost',
    database: 'BDD_compost_aubigny'
  });

  connection.on('connect', () => {
    console.log('Connecté à la base de données MySQL');

    // Exécutez la requête ADD
    connection.query('DELETE FROM `alert_compost` WHERE `id` = 1', (err, results, fields) => {
      if (err) {
        console.error('Erreur lors de l\'exécution de la requête delete :', err);
        throw err;
      }

      // Traitez les résultats de la requête SELECT
      console.log('Résultats de la requête delete :', JSON.stringify(results));
      res.end(JSON.stringify(results));

      // Fermez la connexion à la base de données après avoir traité les résultats
      connection.end();
    });
  });

  // Écoutez l'événement d'erreur de connexion
  connection.on('error', (err) => {
    console.error('Erreur de connexion à la base de données :', err);
  });

  // Connectez-vous à la base de données
  connection.connect();

})

//Handle PUT method
app.patch('/ModifyAlert', function (req, res) {

  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'compost',
    password: 'MdpCompost',
    database: 'BDD_compost_aubigny'
  });

  connection.on('connect', () => {
    console.log('Connecté à la base de données MySQL');

    // Exécutez la requête ADD
    connection.query("UPDATE alert_compost SET nom = '" + req?.body?.nom + "', prenom = '" + req?.body?.prenom + "', email = '" + req?.body?.email + "', tmin = " + req?.body?.tmin + ', tmax = ' + req?.body?.tmax + ', hmin = ' + req?.body?.hmin + ', hmax = ' + req?.body?.hmax + ', nb = ' + req?.body?.nb + ' WHERE `id` = 1', (err, results, fields) => {
      if (err) {
        console.error('Erreur lors de l\'exécution de la requête patch :', err);
        throw err;
      }

      // Traitez les résultats de la requête SELECT
      console.log('Résultats de la requête patch :', JSON.stringify(results));
      res.end(JSON.stringify(results));

      // Fermez la connexion à la base de données après avoir traité les résultats
      connection.end();
    });
  });

  // Écoutez l'événement d'erreur de connexion
  connection.on('error', (err) => {
    console.error('Erreur de connexion à la base de données :', err);
  });

  // Connectez-vous à la base de données
  connection.connect();

});

//Handle POST method
app.post('/Login', function (req, res) {

  if (req?.body?.user == 'admin' && req?.body?.password == 'pass') {
    res.end('test123');
  } else {
    // Mot de passe incorrect
    res.status(401).send('Erreur d\'authentification : Nom d\'utilisateur ou mot de passe incorrect');
  }
})

const server = app.listen(8082, ip.address(), function () {

  const host = server.address().address
  const port = server.address().port

  console.log("Example app listening at http://%s:%s", host, port)

})

