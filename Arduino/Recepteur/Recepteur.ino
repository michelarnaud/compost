/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/ttgo-lora32-sx1276-arduino-ide/
*********/

//Libraries for LoRa
#include <SPI.h>
#include <LoRa.h>
#include <EEPROM.h>
//Libraries for OLED Display
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

//define the pins used by the LoRa transceiver module
#define SCK 5
#define MISO 19
#define MOSI 27
#define SS 18
#define RST 14
#define DIO0 26

//433E6 for Asia
//866E6 for Europe
//915E6 for North America
#define BAND 868E6

//OLED pins
#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16
#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels

#define EEPROM_SIZE 62536

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RST);

String LoRaData;
int counter;
int rssi;

void setup() {
  // put your setup code here, to run once:
  //initialize Serial Monitor
  Serial.begin(115200);

  //reset OLED display via software
  pinMode(OLED_RST, OUTPUT);
  digitalWrite(OLED_RST, LOW);
  delay(20);
  digitalWrite(OLED_RST, HIGH);

  //initialize OLED
  Wire.begin(OLED_SDA, OLED_SCL);
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3c, false, false)) {  // Address 0x3C for 128x32
    //Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;  // Don't proceed, loop forever
  }

  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.setCursor(0, 0);
  display.print("LORA RECEIVER ");
  display.display();
  counter = 0;
  //  Serial.println("LoRa Receiver Test");
  //SPI LoRa pins
  SPI.begin(SCK, MISO, MOSI, SS);
  //setup LoRa transceiver module
  LoRa.setPins(SS, RST, DIO0);

  if (!LoRa.begin(BAND)) {
    //    Serial.println("Starting LoRa failed!");
    while (1)
      ;
  }
  //  Serial.println("LoRa Initializing OK!");
  display.setCursor(0, 10);
  display.println("LoRa Initializing OK!");
  display.display();
  EEPROM.begin(EEPROM_SIZE);
}

void loop() {
  // put your main code here, to run repeatedly:
  //try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {

    // Buffer for incoming data
    char jsonBuffer[255];
    // Read packet into buffer
    int bytesRead = LoRa.readBytes(jsonBuffer, packetSize);
    if (bytesRead > 0) {
      // Check if the first value in JSON is "T1"
      if (jsonBuffer[2] == 'T' && jsonBuffer[3] == '1') {
        // Reset LoRaData
        LoRaData = "";
        // Append received bytes to LoRaData
        for (int i = 0; i < bytesRead; i++) {
          LoRaData += jsonBuffer[i];
        }
        Serial.println(LoRaData);
        counter++;

        //print RSSI of packet
        rssi = LoRa.packetRssi();
        //EEPROM.write(counter%EEPROM_SIZE, rssi);
        // Serial.print(" with RSSI ");
        // Serial.println(rssi);
        // Dsiplay information
        //rssi=EEPROM.read(counter%EEPROM_SIZE);
        display.clearDisplay();
        display.setCursor(0, 0);
        display.print("LORA RECEIVER,Count:");
        display.print(counter);
        display.setCursor(0, 10);
        display.print("RSSI:");
        display.setCursor(30, 10);
        display.print(rssi);
        display.display();
      }
    }
  }
}
