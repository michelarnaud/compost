//Libraries for LoRa
#include <SPI.h>
#include <LoRa.h>

#include <stdio.h>

//Libraries for OLED Display
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

//define the pins used by the LoRa transceiver module
#define SCK 5
#define MISO 19
#define MOSI 27
#define SS 18
#define RST 14
#define DIO0 26

//433E6 for Asia
//866E6 for Europe
//915E6 for North America
#define BAND 868E6

//OLED pins
#define OLED_SDA 4
#define OLED_SCL 15
#define OLED_RST 16
#define SCREEN_WIDTH 128  // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels

#define buttonPin 4


//packet counter
int counter = 0;
int buttonState = 0;

int Temp1 = 10;
int Temp2 = 15;
int Temp3 = 25;
int Temp4 = 40;
int Temp5 = 60;
int Temp6 = 50;
int Tempe = 20;
int H1 = 10;
int H2 = 15;
int H3 = 25;
int H4 = 40;
int H5 = 60;
int H6 = 50;
int He = 20;
int Nb = 50;

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RST);


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  //reset OLED display via software
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(OLED_RST, OUTPUT);
  digitalWrite(OLED_RST, LOW);
  delay(20);
  digitalWrite(OLED_RST, HIGH);

  //initialize OLED
  Wire.begin(OLED_SDA, OLED_SCL);
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3c, false, false)) {  // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;  // Don't proceed, loop forever
  }

  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.setCursor(0, 0);
  display.print("LORA SENDER ");
  display.display();

  Serial.println("LoRa Sender Test");
  SPI.begin(SCK, MISO, MOSI, SS);
  //setup LoRa transceiver module
  LoRa.setPins(SS, RST, DIO0);

  if (!LoRa.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1)
      ;
  }
  LoRa.setTxPower(20);
  Serial.println("LoRa Initializing OK!");
  display.setCursor(0, 10);
  display.print("LoRa Initializing OK!");
  display.display();
  delay(2000);
}

void loop() {

  buttonState = digitalRead(buttonPin);
  Serial.print("bouton: ");
  Serial.println(buttonState);
  // put your main code here, to run repeatedly:
  Serial.print("Sending packet: ");
  Serial.println(counter);

  while (buttonState != LOW) {
    buttonState = digitalRead(buttonPin);
  }

  //Send LoRa packet to receiver
  LoRa.beginPacket();
  LoRa.print("{\"T1\":");
  LoRa.print(Temp1);
  LoRa.print(",\"T2\":");
  LoRa.print(Temp2);
  LoRa.print(",\"T3\":");
  LoRa.print(Temp3);
  LoRa.print(",\"T4\":");
  LoRa.print(Temp4);
  LoRa.print(",\"T5\":");
  LoRa.print(Temp5);
  LoRa.print(",\"T6\":");
  LoRa.print(Temp6);
  LoRa.print(",\"Te\":");
  LoRa.print(Tempe);
  LoRa.print(",\"H1\":");
  LoRa.print(H1);
  LoRa.print(",\"H2\":");
  LoRa.print(H2);
  LoRa.print(",\"H3\":");
  LoRa.print(H3);
  LoRa.print(",\"H4\":");
  LoRa.print(H4);
  LoRa.print(",\"H5\":");
  LoRa.print(H5);
  LoRa.print(",\"H6\":");
  LoRa.print(H6);
  LoRa.print(",\"He\":");
  LoRa.print(He);
  LoRa.print(",\"Nb\":");
  LoRa.print(Nb);
  LoRa.print("}");
  LoRa.endPacket();

  display.clearDisplay();
  display.setCursor(0, 0);
  display.println("LORA SENDER");
  display.setCursor(0, 20);
  display.setTextSize(1);
  display.print("LoRa packet sent.");
  display.setCursor(0, 30);
  display.print("Counter:");
  display.setCursor(50, 30);
  display.print(counter);
  display.display();

  counter++;
  delay(1000);
}
