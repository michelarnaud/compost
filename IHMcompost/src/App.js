import Barre from './Component/navbar';
import './App.css';
import { useNavigate } from "react-router-dom";
import Button from 'react-bootstrap/Button';


function App() {

  const navigate = useNavigate();

  return (
    <div className="App" style={{ fontFamily: "'Montserrat', sans-serif" }}>
      <Barre/>
      <header className="App-header">
            <Button size='lg' variant="outline-success"  onClick={() => navigate("/graphique")}>
            Site d'Aubigny-Les Clouzeaux
            </Button>
      </header>
    </div>
  );
}

export default App;
