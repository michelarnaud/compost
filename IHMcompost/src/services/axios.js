import axios from 'axios';
import { URL_API
 } from '../Constant/config';
const TIMEOUT = 1;

const instance = axios.create({
    baseURL: `${URL_API}`,
    timeout: 1000 * TIMEOUT,      // Timeout de 20/45 secondes (HOMKIA/3GR)
    headers: {
        'Content-Type': 'application/json'
    }
});

instance.interceptors.request.use(
    config => {
        console.log("REQUEST URL WS ==>", config?.url);
        console.log("REQUEST METHOD ==>", config?.method?.toUpperCase());
        console.log("REQUEST HEADERS ==>", config?.headers);
        console.log("REQUEST DATA ==>", config?.data);
        return config;
    },
    error => {
  
        if (error?.message === "Network Error" && error?.name === "Error") {
            console.error('Il se pourrait que votre connexion vers internet ne fonctionne pas.');
        }
    
        console.error("REQUEST ERROR ==>", JSON.stringify(error));
        return Promise.reject(error);
    },
);
  
instance.interceptors.response.use(
     response => {
        console.log("RESPONSE URL WS ==>", response?.config?.url);
        console.log("RESPONSE METHOD ==>", response?.config?.method?.toUpperCase());
        console.log("RESPONSE DATA ==>", response?.data);
        console.log("RESPONSE HEADER ==>", response?.config?.headers?.Authorization);
        return response;
    },
    error => {
        
        console.log("RESPONSE ERROR ==>", error?.response);
        console.log("RESPONSE ERROR URL ==>", error?.response?.config?.url);
        console.log("RESPONSE ERROR STATUS ==>", error?.response?.status);
        console.log("RESPONSE ERROR STATUS TEXT ==>", error?.response?.statusText);
        console.log("RESPONSE ERROR DATA ==>", error?.response?.data);

        if (error?.message === "Network Error" && error?.name === "Error") {
            console.error('Il se pourrait que votre connexion vers internet ne fonctionne pas.');
            return Promise.reject(error);
        } else {
            console.error(error?.data);
        }
        return Promise.reject(error || 'Erreur reçue du serveur');
    },
);

export default instance;
