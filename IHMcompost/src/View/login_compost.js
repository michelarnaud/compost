
import React, { useState, useRef } from 'react';
import Barre from '../Component/navbar';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LoginUser } from '../Store/Login';
import './TemplateLogin.css';


const LoginPage = () => {
  const dispatch = useDispatch();
  const login = useSelector(state => state?.login);
  const inputRef = useRef(null);
  const [error, setError] = useState(login?.errorLogin);
  const [log, setlog] = useState({
    user: '',
    password: '',
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    const temp = { ...log };
    dispatch(LoginUser(temp));
  };

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  useEffect(() => {
    if (login.errorLogin) {
      setError(login.errorLogin);
    }
  }, [login]);

  return (
    <><Barre />
      <div style={{ fontFamily: "'Montserrat', sans-serif" }}>
        <br />
        <div className="login-box">
          {error && <div className="error-message">{error}</div>}
          <h2>Connexion</h2>
          <form onSubmit={handleSubmit}>
            <div className="user-box">
              <input
                type="text"
                name="pseudo"
                value={log.user}
                onChange={(e) => setlog({ ...log, user: e.target.value })}
                autoComplete="off"
                ref={inputRef}
                required
              />
              <label>Utilisateur</label>
            </div>
            <div className="user-box">
              <input
                type="password"
                name="passwd"
                value={log.password}
                onChange={(e) => setlog({ ...log, password: e.target.value })}
                autoComplete="off"
                required
              />
              <label>Mot de passe</label>
            </div>
            <input className="button" type="submit" value="Connexion" autoComplete="off" required />
          </form>
        </div>
      </div>
    </>
  );
};

export default LoginPage;
