import Barre from '../Component/navbar';
import '../App.css';
import React from 'react';
import Card from 'react-bootstrap/Card';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getTemperatureSemaine, getHygrometrieSemaine, getNiveauBatterieInfo } from '../Store/Compost';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';
import { CardBody } from 'react-bootstrap';
import GaugeComponent from 'react-gauge-component'
import styled from 'styled-components';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';
import { GetAlerte, Deletealerte } from '../Store/Alert';
import LoginPage from './login_compost';
import TModal from '../Component/TModal';
import { setTokenCookie, getTokenCookie } from '../Store/Login/cookie';

const Gauge = styled(GaugeComponent)`
  .value-text{
    fill: grey;
    text-shadow: 0 0 grey
  }
`
const CardStyle = styled.div`
  flex-wrap: wrap;
  display : flex;
  justify-content: center;
`
const Graph = styled(LineChart)`
  .recharts-wrapper{
    width: 100%
    height: 100%
  }
`
const TitreGraph = styled.p`
  font-family: Montserrat;
  margin-left: 43px;
  font-size: 20px;
`

function Graphique() {

  const navigate = useNavigate();

  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const compost = useSelector(state => state?.compost);
  const alerteStore = useSelector(state => state?.alert);
  const hygrometrieSemaine = useSelector(state => state?.compost?.hygrometrieSemaine);
  const alerte = useSelector(state => state?.alert?.alerte);
  const id = alerte[0]?.id || null;
  const temperatureSemaine = useSelector(state => state?.compost?.temperatureSemaine);
  const niveaubatterie = useSelector(state => state?.compost?.niveaubatterie);
  const dataFetch = useRef(false);
  const login = useSelector(state => state?.login);
  const cookieToken = getTokenCookie();
  const reverseTemperatureSemaine = temperatureSemaine.slice().reverse();
  const reverseHygrometrieSemaine = hygrometrieSemaine.slice().reverse();


  function getData() {
    if (!compost?.isGetHygrometrieSemaineInProgress && !compost?.isGetTemperatureSemaineInProgress && !compost?.isGetNiveauBatterieInProgress) {
      if (dataFetch.current) {
        return;
      }
      dataFetch.current = true;
      dispatch(getHygrometrieSemaine());
      dispatch(getTemperatureSemaine());
      dispatch(getNiveauBatterieInfo());
      dispatch(GetAlerte());
    }
  }

  useEffect(() => {
    if (!alerteStore.isDeletealerteInProgress && alerteStore.isDeletealerteComplete && !alerteStore.isGetalerteInProgress && !alerteStore.isGetalerteComplete) {
      dispatch(GetAlerte());
    }
  }, [alerteStore]);

  useEffect(() => {
    if (!login.isLoginInProgress && login.isloginComplete) {
      setTokenCookie(login.token);
      getData();
    }
  }, [login]);

  var largeurEcran = window.screen.width;
  const tableauH = ["H1", "H2", "H3", "H4", "H5", "H6", "He"];
  const tableauT = ["T1", "T2", "T3", "T4", "T5", "T6", "Te"]

  if (!login.token) {
    if (!cookieToken) {
      { return <LoginPage /> }
    }else{
      getData();
    }
  }
  return (
    <>
      <Barre showDeconnexionButton={true} />
      {id === null ?
        <Button size='sm' variant="outline-success" style={{ margin: "5px" }} onClick={() => navigate("/alert", { state: { id: null } })}>
          Définir une alerte
        </Button>
        :
        <>
          <Button size='sm' variant="outline-warning" style={{ margin: "5px" }} onClick={() => navigate("/alert", { state: { id: 1 } })}>
            Modifier une alerte
          </Button>
          <Button size='sm' variant="outline-danger" style={{ margin: "5px" }} onClick={() => { setShowModal(true) }}>
            Supprimer une alerte
          </Button>
        </>
      }

      <div className="App">
        <CardStyle>
          <Card style={{ margin: '10px' }}>
            <CardBody style={{ padding: "10px 0px" }}>
              <TitreGraph>Température
                <Button size='sm' variant="light" style={{ margin: "5px" }} onClick={() => navigate("/temp")}>
                  Voir plus
                </Button>
              </TitreGraph>
              <Graph
                width={largeurEcran - largeurEcran / 25}
                height={300}
                data={reverseTemperatureSemaine}
                margin={{
                  top: 10,
                  right: 40,
                  left: 20,
                  bottom: 20,
                }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis
                  dataKey="date"
                  tick=""
                  domain={['auto', 'auto']}
                  interval={1}
                  label={{
                    value: `H`,
                    style: { textAnchor: 'middle' },
                    position: 'right',
                    offset: 0,
                  }}
                  allowDataOverflow={true}
                />
                <YAxis
                  domain={['auto', 'auto']}
                  type="number"
                  interval={0}
                  label={{
                    value: `°C`,
                    style: { textAnchor: 'middle' },
                    position: 'left',
                    offset: 0,
                  }}
                  allowDataOverflow={true} />
                <Tooltip />

                {tableauT.map((key) =>
                  <Line key={key} type="monotone" dataKey={key} stroke="#8884d8" dot={false}/>
                )}
              </Graph>
            </CardBody>
          </Card>
          <Card style={{ margin: '10px' }}>
            <CardBody style={{ padding: "10px 0px" }}><TitreGraph>Hygrométrie
              <Button size='sm' variant="light" style={{ margin: "5px" }} onClick={() => navigate("/humidite")}>
                Voir plus
              </Button>
            </TitreGraph>
              <Graph
                width={largeurEcran - largeurEcran / 25}
                height={300}
                data={reverseHygrometrieSemaine}
                margin={{
                  top: 10,
                  right: 40,
                  left: 20,
                  bottom: 20,
                }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis
                  dataKey="date"
                  tick=""
                  domain={['auto', 'auto']}
                  interval={1}
                  label={{
                    value: `H`,
                    style: { textAnchor: 'middle' },
                    position: 'right',
                    offset: 0,
                  }}
                  allowDataOverflow={true} />
                <YAxis
                  domain={['auto', 'auto']}
                  type="number"
                  interval={0}
                  label={{
                    value: `%`,
                    style: { textAnchor: 'middle' },
                    position: 'left',
                    offset: 0,
                  }}
                  allowDataOverflow={true} />
                <Tooltip />

                {tableauH.map((key) =>
                  <Line key={key} type="monotone" dataKey={key} stroke="#888452" dot={false}/>
                )}
              </Graph>
            </CardBody>
          </Card>
          <Card style={{ width: '560px', margin: '10px' }}>
            <CardBody style={{ padding: "10px 0px" }}><p style={{ fontFamily: "'Montserrat', sans-serif", fontSize: "20px" }}>Batterie</p>
              <GaugeComponent
                arc={{
                  subArcs: [
                    {
                      limit: 20,
                      color: '#EA4228',
                      showTick: true
                    },
                    {
                      limit: 40,
                      color: '#F58B19',
                      showTick: true
                    },
                    {
                      limit: 60,
                      color: '#F5CD19',
                      showTick: true
                    },
                    {
                      limit: 100,
                      color: '#5BE12C',
                      showTick: true
                    },
                  ]
                }}
                value={niveaubatterie[niveaubatterie.length - 1]?.Nb} />
            </CardBody>
          </Card>
        </CardStyle>
      </div>
      <TModal
        isShown={showModal}
        title="Suppression"
        textAsk={`Voules-vous supprimer l'alerte ?`}
        btnTextYes="Oui"
        onClickBtnYes={() => {
          dispatch(Deletealerte());
          setShowModal(false);
        }}
        btnTextNo="Non"
        onClickBtnNo={() => { setShowModal(false); }}
      />
    </>
  );
}

export default Graphique;