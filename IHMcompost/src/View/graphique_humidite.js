import Barre from '../Component/navbar';
import '../App.css';
import { useNavigate } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import styled from 'styled-components';
import Card from 'react-bootstrap/Card';
import { CardBody } from 'react-bootstrap';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getHygrometrieMois } from '../Store/Compost';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, Brush } from 'recharts';
import LoginPage from './login_compost';
import { setTokenCookie, getTokenCookie } from '../Store/Login/cookie';

const CardStyle = styled.div`
  flex-wrap: wrap;
  display : flex;
  justify-content: center;
`
const Graph = styled(LineChart)`
  .recharts-wrapper{
    width: 100%;
    height: 100%;
  }
  .recharts-brush{
    margin-top:50px;
  }
`
const StyleRetour = styled(Button)`
float: right;
margin-right: 35px;
font-size:20px
font-family: Montserrat;
`
const TitreGraph = styled.p`
  font-family: Montserrat;
  margin-left: 43px;
  font-size: 20px;
`
function calculateMedian(arr) {
    const sortedArr = arr.slice().sort((a, b) => a - b);
    const middle = Math.floor(sortedArr.length / 2);
  
    if (sortedArr.length % 2 === 0) {
      return (sortedArr[middle - 1] + sortedArr[middle]) / 2;
    } else {
      return sortedArr[middle];
    }
  }
  
function Humidite() {

  const login = useSelector(state => state?.login);
  const cookieToken = getTokenCookie();
  const navigate = useNavigate();
  const handleRetour = () => {
    // Utilisez history.goBack() pour revenir en arrière
    navigate('/graphique');
  };
  const dispatch = useDispatch();
  const compost = useSelector(state => state?.compost);
  const hygrometrieMois = useSelector(state => state?.compost?.hygrometrieMois);
  const dataFetch = useRef(false);

  useEffect(() => {
    if (!compost?.isGetHygrometrieMoisInProgress) {
      if (dataFetch.current) {
        return;
      }
      dataFetch.current = true;
      dispatch(getHygrometrieMois());
    }
  }, []);
  
  var largeurEcran = window.screen.width;
  const tableauH = ["H1", "H2", "H3", "H4", "H5", "H6", "He"];
  const averageValues = hygrometrieMois.map(entry => {
  const values = tableauH.map(key => entry[key]);
    const Moyenne = values.reduce((acc, value) => acc + value, 0) / values.length;
    const RoundedMoyenne = parseFloat(Moyenne.toFixed(2));
    return { ...entry, Moyenne: RoundedMoyenne };
  });

  const medianValues = hygrometrieMois.map(entry => {
    const values = tableauH.map(key => entry[key]);
    const Mediane = calculateMedian(values);
    return { ...entry, Mediane };
  });
  
  const reverseAverageValues = averageValues.slice().reverse();
  const reverseMedianValues = medianValues.slice().reverse();

  if (!login.token) {
    if (!cookieToken) {
      { return <LoginPage /> }
    }
  }
  return ( 

    <div className="App">
      <Barre showDeconnexionButton={true}/>
        <CardStyle>
          <Card style={{ margin: '10px' }}>
            <CardBody style={{ padding: "10px 0px" }}>
              <TitreGraph>Médiane</TitreGraph>
              <ResponsiveContainer width={largeurEcran - largeurEcran / 25} height={300}>
                <Graph
                data={reverseMedianValues}
                  syncId="anyId"
                  margin={{
                    top: 10,
                    right: 40,
                    left: 20,
                    bottom: 20,
                  }}>
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis
                    dataKey="date"
                    tick=""
                    domain={['auto', 'auto']}
                    interval={1}
                    label={{
                      value: `H`,
                      style: { textAnchor: 'middle' },
                      position: 'right',
                      offset: 0,
                    }}
                    allowDataOverflow={true}
                  />
                  <YAxis
                    domain={['auto', 'auto']}
                    type="number"
                    interval={0}
                    label={{
                      value: `°C`,
                      style: { textAnchor: 'middle' },
                      position: 'left',
                      offset: 0,
                    }}
                    allowDataOverflow={true} />
                  <Tooltip />
                    <Line type="monotone" dataKey="Mediane" stroke="#97BD29" strokeWidth={4} dot={false}/>
                    {tableauH.map((key) =>
                    <Line key={key} type="monotone" dataKey={key} stroke="#7F8C8D" dot={false}/>
                  )}
                </Graph>
              </ResponsiveContainer>
            </CardBody>
          </Card>
          <Card style={{ margin: '10px' }}>
            <CardBody style={{ padding: "10px 0px" }}>
              <TitreGraph>Moyenne</TitreGraph>
              <ResponsiveContainer width={largeurEcran - largeurEcran / 25} height={300}>
                <Graph
                  data={reverseAverageValues}
                  syncId="anyId"
                  margin={{
                    top: 10,
                    right: 40,
                    left: 20,
                    bottom: 20,
                  }}>
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis
                    dataKey="date"
                    tick=""
                    domain={['auto', 'auto']}
                    interval={1}
                    label={{
                      value: `H`,
                      style: { textAnchor: 'middle' },
                      position: 'right',
                      offset: 0,
                    }}
                    allowDataOverflow={true} />
                  <YAxis
                    domain={['auto', 'auto']}
                    type="number"
                    interval={0}
                    label={{
                      value: `%`,
                      style: { textAnchor: 'middle' },
                      position: 'left',
                      offset: 0,
                    }}
                    allowDataOverflow={true} />
                  <Tooltip />
                  <Line type="monotone" dataKey="Moyenne" stroke="#FF5733" strokeWidth={4} dot={false}/>
                    {tableauH.map((key) =>
                    <Line key={key} type="monotone" dataKey={key} stroke="#7F8C8D" dot={false}/>
                  )}
                  <Brush/>
                </Graph>
              </ResponsiveContainer>
            </CardBody>
          </Card>
        </CardStyle>
         <StyleRetour size='sm' variant="secondary" onClick={handleRetour}>
          Retour
        </StyleRetour>
    </div>
  );
}

export default Humidite;
