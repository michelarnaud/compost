import Barre from '../Component/navbar';
import '../App.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import styled from 'styled-components';
import { toast } from 'react-toastify';
import { useNavigate, useLocation } from 'react-router-dom';
import InputGroup from 'react-bootstrap/InputGroup';
import { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Modifyalerte, Addalerte, GetAlerte } from '../Store/Alert';
import LoginPage from './login_compost';
import { getTokenCookie } from '../Store/Login/cookie';

const StyleForm = styled(Form)`
  font-family: Montserrat;
  width:75%;
  font-size: 20px;
  color: green;
  text-align:left;
`
const StyleRetour = styled(Button)`
float: right;
`
const Contenaire = styled.div`
display: flex;
align-items: center;
justify-content: center;
user-select: none;
`
const TitrePerso = styled.p`
margin-top: 10px;
text-align: center;
font-size: 31px;
text-decoration: underline;
margin-bottom: 0rem;
color: #8A6240;
`

function Alert() {

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const dataFetch = useRef(false);
  const [validated, setValidated] = useState(false);
  const location = useLocation();
  const alerteStore = useSelector(state => state?.alert);
  const id = location?.state?.id;
  const login = useSelector(state => state?.login);
  const cookieToken = getTokenCookie();
  const [alerte, setalerte] = useState({
    nom: '',
    prenom: '',
    email: '',
    tmin: null,
    tmax: null,
    hmin: null,
    hmax: null,
    nb: null
  });


  useEffect(() => {
    if (!alerteStore?.isGetalerteInProgress) {
      if (dataFetch.current) {
        return;
      }
      dataFetch.current = true;
      if (id != null) {
        dispatch(GetAlerte());
      }
    }
  }, []);

  useEffect(() => {

    if (!alerteStore.isAddalerteInProgress && alerteStore.isAddalerteComplete) {
      toastSuccess('ajoutée');
      navigate('/graphique');
    }
    if (!alerteStore.isModifyalerteInProgress && alerteStore.isModifyalerteComplete) {
      toastSuccess('modifée');
      navigate('/graphique');
    }

    if (!alerteStore?.isGetalerteInProgress && alerteStore?.isGetalerteComplete) {
      setalerte({
        nom: alerteStore?.alerte[0]?.nom,
        prenom: alerteStore?.alerte[0]?.prenom,
        email: alerteStore?.alerte[0]?.email,
        tmin: alerteStore?.alerte[0]?.tmin,
        tmax: alerteStore?.alerte[0]?.tmax,
        hmin: alerteStore?.alerte[0]?.hmin,
        hmax: alerteStore?.alerte[0]?.hmax,
        nb: alerteStore?.alerte[0]?.nb
      })
    }
  }, [alerteStore]);

  const toastSuccess = (typeOfAlert) => {
    toast.success(`L'alerte à bien été ${typeOfAlert} avec succès`, {
      position: "top-center",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: false,
      pauseOnHover: true,
      draggable: false,
      progress: undefined,
      theme: "dark",
    });
  }

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (!form.checkValidity()) {
      event.preventDefault();
      event.stopPropagation();
    } else if (id == null) {
      event.preventDefault();
      dispatch(Addalerte(alerte));
    } else {
      event.preventDefault();
      const temp = { ...alerte };
      dispatch(Modifyalerte(temp));
    }
    setValidated(true);
  };
  if (!login.token) {
    if(!cookieToken){
      { return <LoginPage /> }
    }
  }
  return (
    <div>
      <Barre />
      <Contenaire>
        <StyleForm noValidate validated={validated} onSubmit={handleSubmit}><TitrePerso>Informations Personnelles</TitrePerso>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Nom et Prénom</Form.Label>
            <InputGroup className="mb-3">
              <Form.Control required value={alerte.nom} onChange={e => { setalerte({ ...alerte, nom: e.target.value }) }} autoComplete='off' placeholder="Nom" />
              <Form.Control required value={alerte.prenom} onChange={e => { setalerte({ ...alerte, prenom: e.target.value }) }} autoComplete='off' placeholder="Prénom" />
            </InputGroup>
            <Form.Label>Adresse Email</Form.Label>
            <Form.Control required value={alerte.email} onChange={e => { setalerte({ ...alerte, email: e.target.value }) }} autoComplete='off' type="email" placeholder="Email" />
            <TitrePerso>Définitition des alertes</TitrePerso>
            <Form.Label>Température</Form.Label>
            <InputGroup className="mb-3">
              <Form.Control required value={alerte.tmin} onChange={e => { setalerte({ ...alerte, tmin: e.target.value }) }} autoComplete='off' placeholder="Minimum" />
              <InputGroup.Text id="basic-addon1">°C</InputGroup.Text>
              <Form.Control required value={alerte.tmax} onChange={e => { setalerte({ ...alerte, tmax: e.target.value }) }} autoComplete='off' placeholder="Maximum" />
              <InputGroup.Text id="basic-addon1">°C</InputGroup.Text>
            </InputGroup>
            <Form.Label>Hygrométrie</Form.Label>
            <InputGroup className="mb-3">
              <Form.Control required value={alerte.hmin} onChange={e => { setalerte({ ...alerte, hmin: e.target.value }) }} autoComplete='off' placeholder="Minimum" />
              <InputGroup.Text id="basic-addon1">%</InputGroup.Text>
              <Form.Control required value={alerte.hmax} onChange={e => { setalerte({ ...alerte, hmax: e.target.value }) }} autoComplete='off' placeholder="Maximum" />
              <InputGroup.Text id="basic-addon1">%</InputGroup.Text>
            </InputGroup>
            <Form.Label>Batterie</Form.Label>
            <InputGroup className="mb-3">
              <Form.Control required value={alerte.nb} onChange={e => { setalerte({ ...alerte, nb: e.target.value }) }} autoComplete='off' placeholder="Batterie" />
              <InputGroup.Text id="basic-addon1">%</InputGroup.Text>
            </InputGroup>
          </Form.Group>

          <Button variant="primary" type="submit">
            Envoyer
          </Button>

          <StyleRetour variant="secondary" onClick={() => { navigate(-1) }}>
            Retour
          </StyleRetour>
        </StyleForm>
      </Contenaire>
    </div>

  );
}

export default Alert;
