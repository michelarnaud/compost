import { Button, Modal } from 'react-bootstrap';
import styled from 'styled-components';

const Styledmodal = styled(Modal)`
    user-select: none;
`

function TModal({
    isShown,
    title,
    textAsk,
    btnTextYes,
    onClickBtnYes,
    btnTextNo,
    onClickBtnNo
}) {
    return (
        <Styledmodal show={isShown} onHide={onClickBtnNo} backdrop="static" keyboard={false} >
            <Modal.Header closeButton>
                <Modal.Title>{title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{textAsk}</Modal.Body>
            <Modal.Footer>
                <Button variant="danger" onClick={onClickBtnYes}>{btnTextYes}</Button>
                <Button variant="primary" onClick={onClickBtnNo}>{btnTextNo}</Button>
            </Modal.Footer>
        </Styledmodal>
    );
}

export default TModal;