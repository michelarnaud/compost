import { ToastContainer } from 'react-toastify';
import styled from 'styled-components';

const StyledToast = styled(ToastContainer)`
    user-select: none;
`

function Toast() {

return (
    <StyledToast
        position="top-center"
        autoClose={3000}
        limit={1}
        hideProgressBar={false}
        closeButton={false}
        newestOnTop={false}
        closeOnClick={false}
        rtl={false}
        pauseOnFocusLoss={false}
        draggable={false}
        pauseOnHover
        theme="dark"
      />
);
}

export default Toast;