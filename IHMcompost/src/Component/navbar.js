import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import styled from 'styled-components';
import Button from 'react-bootstrap/Button';
import { clearTokenCookie } from '../Store/Login/cookie';

const MaNavbar = styled(Navbar)`
.container{
    margin: 0;
    max-width: 2000px;
    user-select: none;
}`
const BrandBouton = styled(Navbar.Brand)`
  margin-left: auto;
`
function Barre({
  showDeconnexionButton
}) {

  return (
    <MaNavbar sticky="top" expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand href="/" style={{ fontFamily: "'Montserrat', sans-serif" }}>
          <img
            src="/Asset/logo.jpg"
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt="logo ALCID"
          />{' '}
          Supervision Compost
        </Navbar.Brand>
        {showDeconnexionButton &&
          <BrandBouton>
            <Button href="/" size='sm' variant="outline-danger" onClick={() => { clearTokenCookie(); }}>
              Déconnexion
            </Button>
          </BrandBouton>}
        <Navbar.Brand>
          <img
            src="/Asset/unnamed.png"
            width="70"
            text-align="right"
            height="40"
            alt="logo SN"
          />
        </Navbar.Brand>

      </Container>
    </MaNavbar>
  );
}

export default Barre;