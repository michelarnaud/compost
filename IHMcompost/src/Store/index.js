import { legacy_createStore as createStore, applyMiddleware, combineReducers } from 'redux';
import { thunk } from 'redux-thunk';
import logger from 'redux-logger';
import { reduxThunkReducer as CompostReducers } from './Compost';
import { reduxThunkReducer as AlertReducers } from './Alert';
import { reduxThunkReducer as LoginReducers } from './Login';


const reducer = combineReducers({
    compost: CompostReducers,
    alert: AlertReducers,
    login: LoginReducers
});


const middleware = applyMiddleware(thunk, logger);

const store = createStore(reducer, middleware);


export default store;