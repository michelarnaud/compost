import axios from '../../services/axios';
import { URL_API } from '../../Constant/config';

export const ALERTE_ADD_LOADING = 'alert/ALERTE_ADD_LOADING';
export const ALERTE_ADD_SUCESS = 'alert/ALERTE_ADD_SUCESS';
export const ALERTE_ADD_ERROR = 'alert/ALERTE_ADD_ERROR';

export const ALERTE_GET_LOADING = 'alert/ALERTE_GET_LOADING';
export const ALERTE_GET_SUCESS = 'alert/ALERTE_GET_SUCESS';
export const ALERTE_GET_ERROR = 'alert/ALERTE_GET_ERROR';

export const ALERTE_MODIFY_LOADING = 'alert/ALERTE_MODIFY_LOADING';
export const ALERTE_MODIFY_SUCESS = 'alert/ALERTE_MODIFY_SUCESS';
export const ALERTE_MODIFY_ERROR = 'alert/ALERTE_MODIFY_ERROR';

export const ALERTE_DELETE_LOADING = 'alert/ALERTE_DELETE_LOADING';
export const ALERTE_DELETE_SUCESS = 'alert/ALERTE_DELETE_SUCESS';
export const ALERTE_DELETE_ERROR = 'alert/ALERTE_DELETE_ERROR';


const initialState = {

    alerte: {},
    isGetalerteInProgress: false,
    isGetalerteComplete: false,
    errorGetalerte: '',

    isAddalerteInProgress: false,
    isAddalerteComplete: false,
    errorAddalerte: '',

    isModifyalerteInProgress: false,
    isModifyalerteComplete: false,
    errorModifyalerte: '',

    isDeletealerteInProgress: false,
    isDeletealerteComplete: false,
    errorDeletealerte: '',

};

export function reduxThunkReducer(state = initialState, action) {
    switch (action.type) {
        case ALERTE_ADD_LOADING: {
            return {
                ...state,
                isAddalerteInProgress: true,
                isAddalerteComplete: false,
                errorAddalerte: '',
            };
        }
        case ALERTE_ADD_SUCESS: {
            return {
                ...state,
                isAddalerteInProgress: false,
                isAddalerteComplete: true,
                isGetalerteComplete: false,
                isModifyalerteComplete: false,
                errorAddalerte: '',
            };
        }
        case ALERTE_ADD_ERROR: {
            return {
                ...state,
                isAddalerteInProgress: false,
                isAddalerteComplete: false,
                errorAddalerte: action?.errorGet || '',
            };
        }
        case ALERTE_MODIFY_LOADING: {
            return {
                ...state,
                isModifyalerteInProgress: true,
                isModifyalerteComplete: false,
                errorModifyalerte: '',
            };
        }
        case ALERTE_MODIFY_SUCESS: {
            return {
                ...state,
                isModifyalerteInProgress: false,
                isModifyalerteComplete: true,
                errorModifyalerte: '',
            };
        }
        case ALERTE_MODIFY_ERROR: {
            return {
                ...state,
                isModifyalerteInProgress: false,
                isModifyalerteComplete: false,
                errorModifyalerte: action?.errorGet || '',
            };
        }
        case ALERTE_DELETE_LOADING: {
            return {
                ...state,
                isDeletealerteInProgress: true,
                isDeletealerteComplete: false,
                errorDeletealerte: '',
            };
        }
        case ALERTE_DELETE_SUCESS: {
            return {
                ...state,
                isDeletealerteInProgress: false,
                isDeletealerteComplete: true,
                isGetalerteComplete: false,
                isAddalerteComplete: false,
                isModifyalerteComplete: false,
                errorDeletealerte: '',
            };
        }
        case ALERTE_DELETE_ERROR: {
            return {
                ...state,
                isDeletealerteInProgress: false,
                isDeletealerteComplete: false,
                errorDeletealerte: action?.errorGet || '',
            };
        }
        case ALERTE_GET_LOADING: {
            return {
                ...state,
                isGetalerteInProgress: true,
                isGetalerteComplete: false,
                alerte: {},
                errorGetalerte: '',
            };
        }
        case ALERTE_GET_SUCESS: {
            return {
                ...state,
                isGetalerteInProgress: false,
                isGetalerteComplete: true,
                isAddalerteComplete: false,
                isModifyalerteComplete: false,
                alerte: action?.data || {},
                errorGetalerte: '',

            };
        }

        case ALERTE_GET_ERROR: {
            return {
                ...state,
                isGetalerteInProgress: false,
                isGetalerteComplete: false,
                alerte: {},
                errorGetalerte: action?.errorGet || '',
            };
        }
        default: {
            return { ...state };
        }
    }
}

////////////////////////////////////////////////////////////////////

export const Modifyalerte = (data) => {
    return (dispatch) => {
        dispatch({
            type: ALERTE_MODIFY_LOADING
        });

        const obj = {
            nom: data?.nom || '',
            prenom: data?.prenom || '',
            email: data?.email || '',
            tmin: data?.tmin || 0,
            tmax: data?.tmax || 0,
            hmin: data?.hmin || 0,
            hmax: data?.hmax || 0,
            nb: data?.nb || 0
        };

        axios.patch(`${URL_API}/ModifyAlert`, obj).then(
            response => {
                console.log("response Hygrometrie data API", response?.data);

                dispatch({
                    type: ALERTE_MODIFY_SUCESS,
                    data: response?.data || [],
                });
            },
            error => {
                console.log("response Hygrometrie error API", error?.response?.data);
                dispatch({ type: ALERTE_MODIFY_ERROR, errorGet: error?.response?.data });
            });
    };
};
export const Addalerte = (data) => {
    return (dispatch) => {
        dispatch({
            type: ALERTE_ADD_LOADING
        });

        const obj = {
            nom: data?.nom || '',
            prenom: data?.prenom || '',
            email: data?.email || '',
            tmin: data?.tmin || 0,
            tmax: data?.tmax || 0,
            hmin: data?.hmin || 0,
            hmax: data?.hmax || 0,
            nb: data?.nb || 0
        };

        axios.post(`${URL_API}/AddAlert`, obj).then(
            response => {
                console.log("response Hygrometrie data API", response?.data);

                dispatch({
                    type: ALERTE_ADD_SUCESS,
                });
            },
            error => {
                console.log("response Hygrometrie error API", error?.response?.data);
                dispatch({ type: ALERTE_ADD_ERROR, errorGet: error?.response?.data });
            });
    };
};
export const Deletealerte = () => {
    return (dispatch) => {
        dispatch({
            type: ALERTE_DELETE_LOADING
        });

        axios.delete(`${URL_API}/DeleteAlert`).then(
            response => {
                console.log("response Hygrometrie data API", response?.data);

                dispatch({
                    type: ALERTE_DELETE_SUCESS,
                });
            },
            error => {
                console.log("response Hygrometrie error API", error?.response?.data);
                dispatch({ type: ALERTE_DELETE_ERROR, errorGet: error?.response?.data });
            });
    };
};
export const GetAlerte = () => {
    return (dispatch) => {
        dispatch({
            type: ALERTE_GET_LOADING
        });

        axios.get(`${URL_API}/Getalert`).then(
            response => {
                console.log("response Temperature data API", response?.data);

                dispatch({
                    type: ALERTE_GET_SUCESS,
                    data: response?.data || {},
                });
            },
            error => {
                console.log("response Temperature error API", error?.response?.data);
                dispatch({ type: ALERTE_GET_ERROR, errorGet: error?.response?.data });
            });
    };
};