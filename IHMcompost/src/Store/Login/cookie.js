
import Cookies from 'js-cookie';

// Récupération du token depuis le cookie
export const getTokenCookie = () => {
  return Cookies.get("token");
};

// Stockage du token dans un cookie
export const setTokenCookie = (token) => {
  Cookies.set('token', token);
}

export const clearTokenCookie = () => {
  Cookies.remove('token');
}
