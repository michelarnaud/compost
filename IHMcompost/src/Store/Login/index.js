import axios from '../../services/axios';
import { URL_API } from '../../Constant/config';

export const LOGIN_LOADING = 'login/LOGIN_LOADING';
export const LOGIN_SUCESS = 'login/LOGIN_SUCESS';
export const LOGIN_ERROR = 'login/LOGIN_ERROR';


const initialState = {

    token: '',
    isLoginInProgress: false,
    isloginComplete: false,
    errorLogin: '',

};

export function reduxThunkReducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_LOADING: {
            return {
                ...state,
                token:'',
                isLoginInProgress: true,
                isloginComplete: false,
                errorLogin: '',
            };
        }
        case LOGIN_SUCESS: {
            return {
                ...state,
                token: action?.data || '',
                isLoginInProgress: false,
                isloginComplete: true,
                errorLogin: '',
            };
        }
        case LOGIN_ERROR: {
            return {
                ...state,
                token:'',
                isLoginInProgress: false,
                isloginComplete: false,
                errorLogin: action?.errorGet || '',
            };
        }
        default: {
            return { ...state };
        }
    }
}

////////////////////////////////////////////////////////////////////

export const LoginUser = (data) => {
    return (dispatch) => {
        dispatch({
            type: LOGIN_LOADING
        });

        const obj = {
            user: data?.user || '',
            password: data?.password || '',
        };

        axios.post(`${URL_API}/Login`, obj).then(
            response => {
                console.log("response login data API", response?.data);

                dispatch({
                    type: LOGIN_SUCESS,
                    data: response?.data || '',
                });
            },
            error => {
                console.log("response login error API", error?.response?.data);
                dispatch({ type: LOGIN_ERROR, errorGet: error?.response?.data });
            });
    };
};
