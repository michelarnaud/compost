import axios from '../../services/axios';
import { URL_API } from '../../Constant/config';

export const HYGROMETRIE_SEMAINE_GET_LOADING = 'compost/HYGROMETRIE_SEMAINE_GET_LOADING';
export const HYGROMETRIE_SEMAINE_GET_SUCCESS = 'compost/HYGROMETRIE_SEMAINE_GET_SUCCESS';
export const HYGROMETRIE_SEMAINE_GET_ERROR = 'compost/HYGROMETRIE_SEMAINE_GET_ERROR';

export const HYGROMETRIE_MOIS_GET_LOADING = 'compost/HYGROMETRIE_MOIS_GET_LOADING';
export const HYGROMETRIE_MOIS_GET_SUCCESS = 'compost/HYGROMETRIE_MOIS_GET_SUCCESS';
export const HYGROMETRIE_MOIS_GET_ERROR = 'compost/HYGROMETRIE_MOIS_GET_ERROR';

export const TEMPERATURE_SEMAINE_GET_LOADING = 'compost/TEMPERATURE_SEMAINE_GET_LOADING';
export const TEMPERATURE_SEMAINE_GET_SUCCESS = 'compost/TEMPERATURE_SEMAINE_GET_SUCCESS';
export const TEMPERATURE_SEMAINE_GET_ERROR = 'compost/TEMPERATURE_SEMAINE_GET_ERROR';

export const TEMPERATURE_MOIS_GET_LOADING = 'compost/TEMPERATURE_MOIS_GET_LOADING';
export const TEMPERATURE_MOIS_GET_SUCCESS = 'compost/TEMPERATURE_MOIS_GET_SUCCESS';
export const TEMPERATURE_MOIS_GET_ERROR = 'compost/TEMPERATURE_MOIS_GET_ERROR';

export const NIVEAUBATTERIE_GET_LOADING = 'compost/NIVEAUBATTERIE_GET_LOADING';
export const NIVEAUBATTERIE_GET_SUCCESS = 'compost/NIVEAUBATTERIE_GET_SUCCESS';
export const NIVEAUBATTERIE_GET_ERROR = 'compost/NIVEAUBATTERIE_GET_ERROR';

const initialState = {

    hygrometrieSemaine: [],
    isGetHygrometrieSemaineInProgress: false,
    isGetHygrometrieSemaineComplete: false,
    errorGetHygrometrieSemaine: '',

    hygrometrieMois: [],
    isGetHygrometrieMoisInProgress: false,
    isGetHygrometrieMoisComplete: false,
    errorGetHygrometrieMois: '',

    temperatureSemaine: [],
    isGetTemperatureSemaineInProgress: false,
    isGetTemperatureSemaineComplete: false,
    errorGetTemperatureSemaine: '',

    temperatureMois: [],
    isGetTemperatureMoisInProgress: false,
    isGetTemperatureMoisComplete: false,
    errorGetTemperatureMois: '',

    niveaubatterie: [],
    isGetNiveauBatterieInProgress: false,
    isGetNiveauBatterieComplete: false,
    errorGetNiveauBatterie: '',

};

export function reduxThunkReducer(state = initialState, action) {
    switch (action.type) {
        case HYGROMETRIE_SEMAINE_GET_LOADING: {
            return {
                ...state,
                isGetHygrometrieSemaineInProgress: true,
                isGetHygrometrieSemaineComplete: false,
                hygrometrieSemaine: [],
                errorGetHygrometrieSemaine: '',
            };
        }
        
        case HYGROMETRIE_SEMAINE_GET_SUCCESS: {
            return {
                ...state,
                isGetHygrometrieSemaineInProgress: false,
                isGetHygrometrieSemaineComplete: true,
                hygrometrieSemaine: action?.data || [],
                errorGetHygrometrieSemaine: '',

            };
        }
        case HYGROMETRIE_SEMAINE_GET_ERROR: {
            return {
                ...state,
                isGetHygrometrieSemaineInProgress: false,
                isGetHygrometrieSemaineComplete: false,
                hygrometrieSemaine: [],
                errorGetHygrometrieSemaine: action?.errorGet || '',
            };
        }
        case HYGROMETRIE_MOIS_GET_LOADING: {
            return {
                ...state,
                isGetHygrometrieMoisInProgress: true,
                isGetHygrometrieMoisComplete: false,
                hygrometrieMois: [],
                errorGetHygrometrieMois: '',
            };
        }
        
        case HYGROMETRIE_MOIS_GET_SUCCESS: {
            return {
                ...state,
                isGetHygrometrieMoisInProgress: false,
                isGetHygrometrieMoisComplete: true,
                hygrometrieMois: action?.data || [],
                errorGetHygrometrieMois: '',

            };
        }
        case HYGROMETRIE_MOIS_GET_ERROR: {
            return {
                ...state,
                isGetHygrometrieMoisInProgress: false,
                isGetHygrometrieMoisComplete: false,
                hygrometrieMois: [],
                errorGetHygrometrieMois: action?.errorGet || '',
            };
        }
        case TEMPERATURE_SEMAINE_GET_LOADING: {
            return {
                ...state,
                isGetTemperatureSemaineInProgress: true,
                isGetTemperatureSemaineComplete: false,
                temperatureSemaine: [],
                errorGetTemperatureSemaine: '',
            };
        }
        case TEMPERATURE_SEMAINE_GET_SUCCESS: {
            return {
                ...state,
                isGetTemperatureSemaineInProgress: false,
                isGetTemperatureSemaineComplete: true,
                temperatureSemaine: action?.data || [],
                errorGetTemperatureSemaine: '',

            };
        }
        case TEMPERATURE_SEMAINE_GET_ERROR: {
            return {
                ...state,
                isGetTemperatureSemaineInProgress: false,
                isGetTemperatureSemaineComplete: false,
                temperatureSemaine: [],
                errorGetTemperatureSemaine: action?.errorGet || '',
            };
        }
        case TEMPERATURE_MOIS_GET_LOADING: {
            return {
                ...state,
                isGetTemperatureMoisInProgress: true,
                isGetTemperatureMoisComplete: false,
                temperatureMois: [],
                errorGetTemperatureMois: '',
            };
        }
        case TEMPERATURE_MOIS_GET_SUCCESS: {
            return {
                ...state,
                isGetTemperatureMoisInProgress: false,
                isGetTemperatureMoisComplete: true,
                temperatureMois: action?.data || [],
                errorGetTemperatureMois: '',

            };
        }
        case TEMPERATURE_MOIS_GET_ERROR: {
            return {
                ...state,
                isGetTemperatureMoisInProgress: false,
                isGetTemperatureMoisComplete: false,
                temperatureMois: [],
                errorGetTemperatureMois: action?.errorGet || '',
            };
        }
        case NIVEAUBATTERIE_GET_LOADING: {
            return {
                ...state,
                isGetNiveauBatterieInProgress: true,
                isGetNiveauBatterieComplete: false,
                niveaubatterie: [],
                errorGetNiveauBatterie: '',
            };
        }
        case NIVEAUBATTERIE_GET_SUCCESS: {
            return {
                ...state,
                isGetNiveauBatterieInProgress: false,
                isGetNiveauBatterieComplete: true,
                niveaubatterie: action?.data || [],
                errorGetNiveauBatterie: '',

            };
        }
        case NIVEAUBATTERIE_GET_ERROR: {
            return {
                ...state,
                isGetNiveauBatterieInProgress: false,
                isGetNiveauBatterieComplete: false,
                niveaubatterie: [],
                errorGetNiveauBatterie: action?.errorGet || '',
            };
        }
        default: {
            return { ...state };
        }
    }
}
////////////////////////////////////////////////////////////////////

export const getHygrometrieSemaine = () => {
    return (dispatch) => {
        dispatch({
            type: HYGROMETRIE_SEMAINE_GET_LOADING
        });

        axios.get(`${URL_API}/HygrometrieSemaine`).then(
            response => {
                console.log("response Hygrometrie data API", response?.data);

                dispatch({
                    type: HYGROMETRIE_SEMAINE_GET_SUCCESS,
                    data: response?.data || [],
                });
            },
            error => {
                console.log("response Hygrometrie error API", error?.response?.data);
                dispatch({ type: HYGROMETRIE_SEMAINE_GET_ERROR, errorGet: error?.response?.data });
            });
    };
};
export const getHygrometrieMois = () => {
    return (dispatch) => {
        dispatch({
            type: HYGROMETRIE_MOIS_GET_LOADING
        });

        axios.get(`${URL_API}/HygrometrieMois`).then(
            response => {
                console.log("response Hygrometrie data API", response?.data);

                dispatch({
                    type: HYGROMETRIE_MOIS_GET_SUCCESS,
                    data: response?.data || [],
                });
            },
            error => {
                console.log("response Hygrometrie error API", error?.response?.data);
                dispatch({ type: HYGROMETRIE_MOIS_GET_ERROR, errorGet: error?.response?.data });
            });
    };
};
export const getTemperatureSemaine = () => {
    return (dispatch) => {
        dispatch({
            type: TEMPERATURE_SEMAINE_GET_LOADING
        });

        axios.get(`${URL_API}/TemperatureSemaine`).then(
            response => {
                console.log("response Temperature data API", response?.data);

                dispatch({
                    type: TEMPERATURE_SEMAINE_GET_SUCCESS,
                    data: response?.data || [],
                });
            },
            error => {
                console.log("response Temperature error API", error?.response?.data);
                dispatch({ type: TEMPERATURE_SEMAINE_GET_ERROR, errorGet: error?.response?.data });
            });
    };
};
export const getTemperatureMois = () => {
    return (dispatch) => {
        dispatch({
            type: TEMPERATURE_MOIS_GET_LOADING
        });

        axios.get(`${URL_API}/TemperatureMois`).then(
            response => {
                console.log("response Temperature data API", response?.data);

                dispatch({
                    type: TEMPERATURE_MOIS_GET_SUCCESS,
                    data: response?.data || [],
                });
            },
            error => {
                console.log("response Temperature error API", error?.response?.data);
                dispatch({ type: TEMPERATURE_MOIS_GET_ERROR, errorGet: error?.response?.data });
            });
    };
};
export const getNiveauBatterieInfo = () => {
    return (dispatch) => {
        dispatch({
            type: NIVEAUBATTERIE_GET_LOADING
        });

        axios.get(`${URL_API}/NiveauBatterie`).then(
            response => {
                console.log("response NiveauBatterie data API", response?.data);

                dispatch({
                    type: NIVEAUBATTERIE_GET_SUCCESS,
                    data: response?.data || [],
                });
            },
            error => {
                console.log("response NiveauBatterie error API", error?.response?.data);
                dispatch({ type: NIVEAUBATTERIE_GET_ERROR, errorGet: error?.response?.data });
            });
    };
};


