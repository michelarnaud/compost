import '../App.css';
import Home from '../App';
import Graphique from '../View/graphique_compost';
import Alert from '../View/alert_compost';
import Temp from '../View/graphique_temperature';
import Humidite from '../View/graphique_humidite';

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/graphique",
    element: <Graphique />,
  },
  {
    path: "/alert",
    element: <Alert />,
  },
  {
    path: "/temp",
    element: <Temp />,
  },
  {
    path: "/humidite",
    element: <Humidite />,
  }
]);

function Router() {
  return (
    <RouterProvider router={router} />
  );
}

export default Router;